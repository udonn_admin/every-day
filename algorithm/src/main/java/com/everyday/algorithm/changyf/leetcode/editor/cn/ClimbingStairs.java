//假设你正在爬楼梯。需要 n 阶你才能到达楼顶。 
//
// 每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？ 
//
// 注意：给定 n 是一个正整数。 
//
// 示例 1： 
//
// 输入： 2
//输出： 2
//解释： 有两种方法可以爬到楼顶。
//1.  1 阶 + 1 阶
//2.  2 阶 
//
// 示例 2： 
//
// 输入： 3
//输出： 3
//解释： 有三种方法可以爬到楼顶。
//1.  1 阶 + 1 阶 + 1 阶
//2.  1 阶 + 2 阶
//3.  2 阶 + 1 阶
// 
// Related Topics 动态规划 
// 👍 1345 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class ClimbingStairs {
    public static void main(String[] args) {
        Solution solution = new ClimbingStairs().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //台阶数 1 2 3 4 5
    //方法数 1 2 3 5 8
    //因为每次只可以爬1阶或2阶，当前台阶数n的方法数就是台阶数n-2爬2阶，或者台阶数n-1爬1阶
    //可以得到f(n) = f(n-1) + f(n-2) ,就是斐波那契数列
    public int climbStairs(int n) {
        if (n < 4){
            return n;
        }
        int first = 2;
        int second = 3;
        int result = 0;
        for (int i = 4; i <= n; i++) {
            result = first + second;
            first = second;
            second = result;
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}