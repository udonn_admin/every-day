//给定一个只包括 '('，')'，'{'，'}'，'['，']' 的字符串，判断字符串是否有效。 
//
// 有效字符串需满足： 
//
// 
// 左括号必须用相同类型的右括号闭合。 
// 左括号必须以正确的顺序闭合。 
// 
//
// 注意空字符串可被认为是有效字符串。 
//
// 示例 1: 
//
// 输入: "()"
//输出: true
// 
//
// 示例 2: 
//
// 输入: "()[]{}"
//输出: true
// 
//
// 示例 3: 
//
// 输入: "(]"
//输出: false
// 
//
// 示例 4: 
//
// 输入: "([)]"
//输出: false
// 
//
// 示例 5: 
//
// 输入: "{[]}"
//输出: true 
// Related Topics 栈 字符串 
// 👍 1982 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;

import java.util.Stack;

public class ValidParentheses {
    public static void main(String[] args) {
        Solution solution = new ValidParentheses().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //遇到左括号压入栈，遇到右括号首先判断栈是否空，然后判断
    //是否为对应的括号，栈空或者不是对应括号，返回false
    public boolean isValid(String s) {
        boolean result = true;
        Stack<Character> stack = new Stack<Character>();
        for (int i=0; i<s.length(); i++){
            if (result == false){
                break;
            }
            Character temp = s.charAt(i);
            switch (temp){
                case '{':
                case '[':
                case '(':
                    stack.push(temp);
                    break;
                case ')':
                    if (stack.isEmpty() || stack.pop() != '('){
                        result = false;
                    }
                    break;
                case ']':
                    if (stack.isEmpty() || stack.pop() != '['){
                        result = false;
                    }
                    break;
                case '}':
                    if (stack.isEmpty() || stack.pop() != '{'){
                        result = false;
                    }
                    break;
                default:
                    break;
            }
        }
        if (!stack.isEmpty()){
            result = false;
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}