//给定一个仅包含大小写字母和空格 ' ' 的字符串 s，返回其最后一个单词的长度。如果字符串从左向右滚动显示，那么最后一个单词就是最后出现的单词。 
//
// 如果不存在最后一个单词，请返回 0 。 
//
// 说明：一个单词是指仅由字母组成、不包含任何空格字符的 最大子字符串。 
//
// 
//
// 示例: 
//
// 输入: "Hello World"
//输出: 5
// 
// Related Topics 字符串 
// 👍 258 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class LengthOfLastWord {
    public static void main(String[] args) {
        Solution solution = new LengthOfLastWord().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int lengthOfLastWord(String s) {
        //初始化为-1，s字符串长度可能<=1
        int end = -1;
        int start = -1;

        int i=s.length() - 1;
        //判断当前应该移动的指针
        boolean isStart = false;
        while (i >= 0){
            //end，如果字符不是' '就是end的结束位置
            if (!isStart && s.charAt(i) != ' '){
                end = i;
                isStart = !isStart;
            }
            //此时切换到start，如果字符是' '就是start的位置
            if (isStart && s.charAt(i) == ' '){
                start = i;
                break;
            }
            i--;
        }
        return end - start;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}