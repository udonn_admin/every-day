//给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。 
//
// 最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。 
//
// 你可以假设除了整数 0 之外，这个整数不会以零开头。 
//
// 
//
// 示例 1： 
//
// 
//输入：digits = [1,2,3]
//输出：[1,2,4]
//解释：输入数组表示数字 123。
// 
//
// 示例 2： 
//
// 
//输入：digits = [4,3,2,1]
//输出：[4,3,2,2]
//解释：输入数组表示数字 4321。
// 
//
// 示例 3： 
//
// 
//输入：digits = [0]
//输出：[1]
// 
//
// 
//
// 提示： 
//
// 
// 1 <= digits.length <= 100 
// 0 <= digits[i] <= 9 
// 
// Related Topics 数组 
// 👍 582 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class PlusOne {
    public static void main(String[] args) {
        Solution solution = new PlusOne().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int[] plusOne(int[] digits) {
        int len = digits.length - 1;
        digits[len] += 1;
        //如果当前值等于10，则进1，否则结束循环；
        while (len>0 && digits[len]==10){
            digits[len] = 0;
            digits[len - 1] += 1;
            len --;
        }
        //如果第一位等于10，进1的同时需要复制到一个新的数组
        if (digits[0] == 10){
            int[] digits2 = new int[digits.length +1];
            digits2[0] = 1;
            digits2[1] = 0;
            for (int i = 1; i < digits.length; i++) {
                digits2[i+1] = digits[i];
            }
            digits = digits2;
        }
        return digits;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}