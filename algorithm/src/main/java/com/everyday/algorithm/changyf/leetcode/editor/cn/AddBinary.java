//给你两个二进制字符串，返回它们的和（用二进制表示）。 
//
// 输入为 非空 字符串且只包含数字 1 和 0。 
//
// 
//
// 示例 1: 
//
// 输入: a = "11", b = "1"
//输出: "100" 
//
// 示例 2: 
//
// 输入: a = "1010", b = "1011"
//输出: "10101" 
//
// 
//
// 提示： 
//
// 
// 每个字符串仅由字符 '0' 或 '1' 组成。 
// 1 <= a.length, b.length <= 10^4 
// 字符串如果不是 "0" ，就都不含前导零。 
// 
// Related Topics 数学 字符串 
// 👍 521 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class AddBinary {
    public static void main(String[] args) {
        Solution solution = new AddBinary().new Solution();
        solution.addBinary("1010","1011");
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public String addBinary(String a, String b) {
        //短字符串
        String sStr;
        //长字符串
        String lStr;
        if (a.length() > b.length()){
            lStr = a;
            sStr = b;
        } else {
            lStr = b;
            sStr = a;
        }
        int lLen = lStr.length();
        int sLen = sStr.length();
        StringBuilder res = new StringBuilder(lLen+1);
        int temp = 0;
        for (int i = 0; i < lLen; i++) {
            temp += lStr.charAt(lLen-1-i) - '0';
            if(i < sLen){
                temp += sStr.charAt(sLen-1-i) - '0';
            }
            res.append(temp % 2);
            temp /= 2;
        }
        if (temp > 0){
            res.append(temp);
        }
        return res.reverse().toString();
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}