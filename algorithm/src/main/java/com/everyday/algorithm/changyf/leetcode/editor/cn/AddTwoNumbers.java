
//给出两个 非空 的链表用来表示两个非负的整数。其中，它们各自的位数是按照 逆序 的方式存储的，并且它们的每个节点只能存储 一位 数字。 
//
// 如果，我们将这两个数相加起来，则会返回一个新的链表来表示它们的和。 
//
// 您可以假设除了数字 0 之外，这两个数都不会以 0 开头。 
//
// 示例： 
//
// 输入：(2 -> 4 -> 3) + (5 -> 6 -> 4)
//输出：7 -> 0 -> 8
//原因：342 + 465 = 807
// 
// Related Topics 链表 数学 
// 👍 4851 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class AddTwoNumbers {
    public static void main(String[] args) {
        Solution solution = new AddTwoNumbers().new Solution();
        ListNode l1 = new AddTwoNumbers().new ListNode(2);
        ListNode l2 = new AddTwoNumbers().new ListNode(5);
        l1.next = new AddTwoNumbers().new ListNode(5);
        l2.next = new AddTwoNumbers().new ListNode(5);

        ListNode res = solution.addTwoNumbers(l1,l2);
    }
//leetcode submit region begin(Prohibit modification and deletion)
/**
 * Definition for singly-linked list.
 * public class ListNode {
 *     int val;
 *     ListNode next;
 *     ListNode(int x) { val = x; }
 * }
 */
public class ListNode {
     int val;
     ListNode next;
     ListNode(int x) { val = x; }
}
class Solution {
    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        //以l1为主链
        ListNode result = l1;
        int temp = 0;
        while (true){
            //将结果存入主链l1中
            temp += l1.val + l2.val;
            l1.val = temp%10;
            temp = temp/10;
            //当l1的长度小于等于l2的长度时，将l2剩余的链连接到l1后面
            if (l1.next == null){
                l1.next = l2.next;
                break;
            } else if (l2.next == null){
                break;
            } else {
                l1 = l1.next;
                l2 = l2.next;
            }
        }
        //如果l1尾部还有链，继续进行计算，同上
        if (l1.next != null){
            //当不再进1或l1尾部无链，退出循环
            while (temp > 0 && l1.next != null){
                l1 = l1.next;
                temp += l1.val;
                l1.val = temp%10;
                temp = temp/10;
            }
        }
        //如果到这里还需要进1，则新创建一个块到l1的尾部
        if (temp >0){
            l1.next = new ListNode(temp);
            /* 不创建新的块，复用块
            l1.next = l2;
            l2.val = temp;
            l2.next = null;
            */
        }
        return result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}