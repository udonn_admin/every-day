//给定一个字符串，验证它是否是回文串，只考虑字母和数字字符，可以忽略字母的大小写。 
//
// 说明：本题中，我们将空字符串定义为有效的回文串。 
//
// 示例 1: 
//
// 输入: "A man, a plan, a canal: Panama"
//输出: true
// 
//
// 示例 2: 
//
// 输入: "race a car"
//输出: false
// 
// Related Topics 双指针 字符串 
// 👍 298 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class ValidPalindrome {
    public static void main(String[] args) {
        Solution solution = new ValidPalindrome().new Solution();
        solution.isPalindrome("A man, a plan, a canal: Panama");
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public boolean isPalindrome(String s) {
        if (s.length() == 0){
            return true;
        }
        char c1;
        char c2;
        int p1 = findChar(s,0);
        int p2 = findLastChar(s,s.length() - 1);
        while (p2>p1){
            c1 = s.charAt(p1);
            c2 = s.charAt(p2);
            if (c1 != c2){
                if (c1>='A' && c2>='A'){
                    if (Math.abs(c1-c2) % 32 != 0){
                        return false;
                    }
                }else {
                    return false;
                }
            }
            p1 = findChar(s,++p1);
            p2 = findLastChar(s,--p2);
        }
        return true;
    }

    private int findChar(String str, int index){
        char c = str.charAt(index);
        while (index<str.length()-1 && ('0'>c || (c>'9' && c<'A') || (c>'Z' && c<'a') || c>'z')){
            index++;
            c = str.charAt(index);
        }
        return index;
    }
    private int findLastChar(String str, int index){
        char c = str.charAt(index);
        while (index>0 && ('0'>c || (c>'9' && c<'A') || (c>'Z' && c<'a') || c>'z')){
            index--;
            c = str.charAt(index);
        }
        return index;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}