//给定一个数组，它的第 i 个元素是一支给定股票第 i 天的价格。 
//
// 如果你最多只允许完成一笔交易（即买入和卖出一支股票一次），设计一个算法来计算你所能获取的最大利润。 
//
// 注意：你不能在买入股票前卖出股票。 
//
// 
//
// 示例 1: 
//
// 输入: [7,1,5,3,6,4]
//输出: 5
//解释: 在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
//     注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格；同时，你不能在买入前卖出股票。
// 
//
// 示例 2: 
//
// 输入: [7,6,4,3,1]
//输出: 0
//解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。
// 
// Related Topics 数组 动态规划 
// 👍 1311 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;

public class BestTimeToBuyAndSellStock {
    public static void main(String[] args) {
        Solution solution = new BestTimeToBuyAndSellStock().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //买入时的价格越低越好，卖出时的价格越高越好。
    //买入后，后一天的价格如果大于买入价格，计算卖出价格，如果低于那么应该这次买入，再继续计算。
    //假设后面有更高价格，那么当前的买入价格越低越好。
    //重新计算买入价格后，前面的所有交易都与以后的交易无关了。
    public int maxProfit(int[] prices) {
        if(prices == null || prices.length < 2) {
            return 0;
        }
        //最大收益，越大越好
        int dp0 = 0;
        //买入, 绝对值越小越好
        int dp1 = -prices[0];
        for (int i = 1; i < prices.length; i++) {
            dp0 = Math.max(dp0, prices[i] + dp1);
            dp1 = Math.max(dp1, -prices[i]);
        }
        return dp0;
    }
//    public int maxProfit(int[] prices) {
//        if(prices==null || prices.length==0) {
//            return 0;
//        }
//        int n = prices.length;
//        //初始化第一天的值
//        int dp0 = 0;
//        int dp1 = -prices[0];
//        for(int i=1;i<n;++i) {
//            //dp0表示第i天卖出的最大收益
//            dp0 = Math.max(dp0,dp1+prices[i]);
//            //dp1表示第i天买入的最大收益
//            dp1 = Math.max(dp1,-prices[i]);
//        }
//        return Math.max(dp0,dp1);
//    }
}
//leetcode submit region end(Prohibit modification and deletion)

}