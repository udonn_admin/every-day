
//给出一个 32 位的有符号整数，你需要将这个整数中每位上的数字进行反转。 
//
// 示例 1: 
//
// 输入: 123
//输出: 321
// 
//
// 示例 2: 
//
// 输入: -123
//输出: -321
// 
//
// 示例 3: 
//
// 输入: 120
//输出: 21
// 
//
// 注意: 
//
// 假设我们的环境只能存储得下 32 位的有符号整数，则其数值范围为 [−231, 231 − 1]。请根据这个假设，如果反转后整数溢出那么就返回 0。 
// Related Topics 数学 
// 👍 2161 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class ReverseInteger {

    public static void main(String[] args) {
        Solution solution = new ReverseInteger().new Solution();
        solution.reverse(-2147483648);
    }

//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    final int [] sizeTable = { 9, 99, 999, 9999, 99999, 999999, 9999999,
            99999999, 999999999, Integer.MAX_VALUE };
    //Integer的stringSize方法
    int stringSize(int x) {
        for (int i=0; ; i++){
            if (x <= sizeTable[i]){
                return i+1;
            }
        }
    }
    public int reverse(int x) {
        //如果是最小值，直接返回0
        if (x == Integer.MIN_VALUE){
            return 0;
        }
        //用来做负数标志
        boolean flag = false;
        //如果小于零先转为正数，并标记为负数
        if (x < 0){
            x=-x;
            flag = true;
        }
        //取x的长度
        int size = stringSize(x);
        long result = 0L;
        for (int i=size-1; i>=0; i--){
            //每次循环判断是否溢出，溢出直接返回0
            if (result > Integer.MAX_VALUE){
                return 0;
            }
            result += x%10 * Math.pow(10,i);
            x = x/10;
        }
        if (flag){
            result = -result;
        }
        return (int)result;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}