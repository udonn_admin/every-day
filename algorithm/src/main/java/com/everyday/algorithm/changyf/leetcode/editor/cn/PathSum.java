//给定一个二叉树和一个目标和，判断该树中是否存在根节点到叶子节点的路径，这条路径上所有节点值相加等于目标和。 
//
// 说明: 叶子节点是指没有子节点的节点。 
//
// 示例: 
//给定如下二叉树，以及目标和 sum = 22， 
//
//               5
//             / \
//            4   8
//           /   / \
//          11  13  4
//         /  \      \
//        7    2      1
// 
//
// 返回 true, 因为存在目标和为 22 的根节点到叶子节点的路径 5->4->11->2。 
// Related Topics 树 深度优先搜索 
// 👍 466 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class PathSum {
    public static void main(String[] args) {
        Solution solution = new PathSum().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)

public class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}

class Solution {
    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        // 到达叶子节点时，递归终止，判断 sum 是否符合条件。
        if (root.left == null && root.right == null) {
            return root.val == sum;
        }
        // 递归地判断root节点的左孩子和右孩子。
        return hasPathSum(root.left, sum - root.val) || hasPathSum(root.right, sum - root.val);
//        return tra(root, 0, sum);
    }

    /**
     * 判断二叉树路径是否存在目标值sum
     * @param root 树节点
     * @param centSum 路径上值的和
     * @param sum 目标值
     * @return
     */
    public boolean tra(TreeNode root, int centSum, int sum){
        centSum += root.val;
        if (root.left == null && root.right == null){
            return centSum == sum;
        }else if (root.left == null){
            return tra(root.right, centSum, sum);
        }else if (root.right == null){
            return tra(root.left, centSum, sum);
        }else {
            return tra(root.left, centSum, sum) || tra(root.right, centSum, sum);
        }

    }
}
//leetcode submit region end(Prohibit modification and deletion)

}