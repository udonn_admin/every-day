//给定一个整数数组 nums ，找到一个具有最大和的连续子数组（子数组最少包含一个元素），返回其最大和。 
//
// 示例: 
//
// 输入: [-2,1,-3,4,-1,2,1,-5,4]
//输出: 6
//解释: 连续子数组 [4,-1,2,1] 的和最大，为 6。
// 
//
// 进阶: 
//
// 如果你已经实现复杂度为 O(n) 的解法，尝试使用更为精妙的分治法求解。 
// Related Topics 数组 分治算法 动态规划 
// 👍 2636 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class MaximumSubarray {
    public static void main(String[] args) {
        Solution solution = new MaximumSubarray().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //我的解法，思想是用数组temp[]记录nums[]从0到当前元素所有的连续子数组的值,然后记录最大值
    //temp[n] = temp[n](上一次) + nums[n] o(n²)
//    public int maxSubArray(int[] nums) {
//        int maxNum = nums[0];
//        int temp[] = new int[nums.length];
//        temp[0] = nums[0];
//        for (int i = 1; i < nums.length; i++) {
//            for (int j = 0; j <= i; j++) {
//                temp[j] = temp[j] + nums[i];
//                if (maxNum < temp[j]){
//                    maxNum = temp[j];
//                }
//            }
//
//        }
//        return maxNum;
//    }
    //看了题解后：根据sum(n) = sum(n-1) + n，可以得到，sum(n)主要受sum(n-1)影响，sum(n-1)>0对结果有帮助，
    //否则应该抛弃。
    public int maxSubArray(int[] nums) {
        //记录最大值
        int maxNum = nums[0];
        //记录连续子数组的最大和，相当于sum(n-1)
        int pre = nums[0];
        for (int i = 1; i < nums.length; i++) {
            //如果pre大于0对结果有帮助
            pre = pre > 0 ? pre+nums[i] : nums[i];
            maxNum = Math.max(pre, maxNum);
        }
        return maxNum;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}