
//给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。 
//
// 示例 1: 
//
// 输入: "abcabcbb"
//输出: 3 
//解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
// 
//
// 示例 2: 
//
// 输入: "bbbbb"
//输出: 1
//解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
// 
//
// 示例 3: 
//
// 输入: "pwwkew"
//输出: 3
//解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
//     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
// 
// Related Topics 哈希表 双指针 字符串 Sliding Window 
// 👍 4274 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;


public class LongestSubstringWithoutRepeatingCharacters {
    public static void main(String[] args) {
        Solution solution = new LongestSubstringWithoutRepeatingCharacters().new Solution();
    }
//leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    public int lengthOfLongestSubstring(String s) {
        int len = s.length();
        //如果字符串长度不大于1，直接返回
        if (len <= 1 ){
            return len;
        }
        //最长子串长度
        int max = 0;
        //当前子串的起始下标
        int index = 0;
        int temp;
        for (int i=0; i<len; i++){
            //从index开始查找下标为i的字符的第1个下标位置
            temp = s.indexOf(s.charAt(i),index);
            //如果找到的第1个下标是自身，说明没有重复
            if(i == temp){
                continue;
            }
            //如果找到的第1个下标不是自身，计算长度
            if (max < i - index){
                max = i - index ;
            }
            //将index重置为重复字符的后1个下标
            index = temp+1;
        }
        //计算最后1个子串的长度
        if (max < len - index){
            max = len - index ;
        }
        return max;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}