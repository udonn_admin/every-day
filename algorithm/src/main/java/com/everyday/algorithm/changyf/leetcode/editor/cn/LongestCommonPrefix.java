//编写一个函数来查找字符串数组中的最长公共前缀。 
//
// 如果不存在公共前缀，返回空字符串 ""。 
//
// 示例 1: 
//
// 输入: ["flower","flow","flight"]
//输出: "fl"
// 
//
// 示例 2: 
//
// 输入: ["dog","racecar","car"]
//输出: ""
//解释: 输入不存在公共前缀。
// 
//
// 说明: 
//
// 所有输入只包含小写字母 a-z 。 
// Related Topics 字符串 
// 👍 1348 👎 0

package com.everyday.algorithm.changyf.leetcode.editor.cn;
public class LongestCommonPrefix {
    public static void main(String[] args) {
        Solution solution = new LongestCommonPrefix().new Solution();
    }
    //leetcode submit region begin(Prohibit modification and deletion)
class Solution {
    //先找出最短的字符串，然后和其他字符串对比找出最长公共前缀
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0){
            return "";
        }
        int min = Integer.MAX_VALUE;
        String prefixStr = "";
        for (String item : strs){
            if (item.length() < min){
                min = item.length();
                prefixStr = item;
            }
        }
        for (String item : strs){
            if ("".equals(prefixStr)){
                break;
            }
            for (int i = 0; i < prefixStr.length(); i++){
                if (item.charAt(i) != prefixStr.charAt(i)){
                    if (i == 0){
                        prefixStr = "";
                        break;
                    }
                    prefixStr = prefixStr.substring(0,i);
                }
            }
        }
        return prefixStr;
    }
}
//leetcode submit region end(Prohibit modification and deletion)

}